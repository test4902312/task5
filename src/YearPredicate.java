public class YearPredicate implements MoviePredicate {
    private int year;

    public YearPredicate(int year) {
        this.year = year;
    }
    @Override
    public boolean test(Movie movie) {
        return movie.getYear() == year;
    }
}
