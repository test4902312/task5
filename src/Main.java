import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie("Inception", "Sci-Fi", "Christopher Nolan", 2010, 8.8));
        movies.add(new Movie("The Shawshank Redemption", "Drama", "Frank Darabont", 1994, 9.3));
        movies.add(new Movie("Pulp Fiction", "Crime", "Quentin Tarantino", 1994, 8.9));
        movies.add(new Movie("The Dark Knight", "Action", "Christopher Nolan", 2008, 9.0));
        movies.add(new Movie("Fight Club", "Drama", "David Fincher", 1999, 8.8));
        movies.add(new Movie("The Godfather", "Crime", "Francis Ford Coppola", 1972, 9.2));
        movies.add(new Movie("Interstellar", "Sci-Fi", "Christopher Nolan", 2014, 8.6));
        movies.add(new Movie("Forrest Gump", "Drama", "Robert Zemeckis", 1994, 8.8));
        movies.add(new Movie("Goodfellas", "Crime", "Martin Scorsese", 1990, 8.7));
        movies.add(new Movie("The Matrix", "Sci-Fi", "Lana Wachowski, Lilly Wachowski", 1999, 8.7));

//        Filter movies by year without using predicates
        List filteredByYear = Filters.filterByYear(movies,1999);
        System.out.println("Movies in year 1999 "+filteredByYear);

//        Filter movies by genres using a custom predicate
        MoviePredicate crimePredicate = new GenrePredicate("Drama");
        List crimeMovies = Filters.filterByPredicate(movies, crimePredicate);
        System.out.println("Crime movies "+crimeMovies);

//        Filter movies by movie year using a custom predicate
        int MOVIE_YEAR = 2008;
        MoviePredicate yearPredicate = new YearPredicate(MOVIE_YEAR);
        List moviesByYear = Filters.filterByPredicate(movies , yearPredicate);
        System.out.println("Movies in year "+MOVIE_YEAR);
        System.out.println(moviesByYear);

//        Filter movies by rating using a stream operation
        List<Movie> filteredByRating = movies.stream().filter(m -> m.getRating() > 8.5).collect(Collectors.toList());
        System.out.println("Filtered by rating");
        System.out.println(filteredByRating);

//        Filter movies by director using a parallel stream operation
        List<Movie> filterByDirector = movies.parallelStream().filter(m ->m.getDirector() == "Christopher Nolan").collect(Collectors.toList());
        System.out.println("Filter by director");
        System.out.println(filterByDirector);
    }
}
