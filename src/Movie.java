public class Movie {
    private String title;
    private String genre;
    private String director;
    private int year;
    private double rating;

    public Movie(String title, String genre, String director, int year, double rating) {
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.year = year;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public int getYear() {
        return year;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "Title: " + title +
                ", Genre: " + genre +
                ", Director: " + director +
                ", Year: " + year +
                ", Rating: " + rating;
    }
}
