public interface MoviePredicate {
    boolean test(Movie movie);
}
