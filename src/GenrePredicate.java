public class GenrePredicate implements MoviePredicate {

    private String genre;

    public GenrePredicate(String genre) {
        this.genre = genre;
    }
    @Override
    public boolean test(Movie movie) {
        return movie.getGenre().equals(genre);
    }
}
