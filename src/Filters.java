import java.util.LinkedList;
import java.util.List;

public class Filters {
    public static List<Movie> filterByYear(List<Movie> movies, int year){
        List<Movie> result = new LinkedList<Movie>();
        for(Movie movie: movies){
            if(movie.getYear() == year){
                result.add(movie);
            }
        }
        return result;
    }

    public static List<Movie> filterByPredicate(List<Movie> movies, MoviePredicate predicate){
        List<Movie> result = new LinkedList<Movie>();
        for(Movie movie: movies){
            if(predicate.test(movie)){
                result.add(movie);
            }
        }
        return result;
    }

}
